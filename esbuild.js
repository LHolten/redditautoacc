require("esbuild").buildSync({
  entryPoints: ["src/index.ts"],
  bundle: true,
  loader: { ".txt": "text" },
  platform: "node",
  format: "cjs",
  external: ["selenium-webdriver", "@cemalgnlts/mailjs"],
  outfile: "dist/bundle.js",
  sourcemap: "inline",
  minify: true,
});
