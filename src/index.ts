import {
  Builder,
  By,
  Capabilities,
  Key,
  until,
  WebDriver,
} from "selenium-webdriver";
import inquirer from "inquirer";
import Mailjs from "@cemalgnlts/mailjs";
//@ts-ignore - esbuild lets us import files
import names from "./names.txt";
import { readFileSync, writeFileSync } from "fs";
const namelist = names.split(/\r?\n/);

function wait(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

function getUsername() {
  return (
    namelist[Math.round(Math.random() * namelist.length)] +
    Math.random().toString(36).substring(7)
  ).toLowerCase();
}
function getPassword() {
  return (
    Math.random().toString(36).substring(7) +
    Math.random().toString(36).substring(7) +
    Math.random().toString(36).substring(7)
  );
}
async function getEmail(mailjs) {
  if (process.env.CUSTOMEMAIL) {
    return await inquirer
      .prompt([
        {
          name: "email",
          type: "input",
          message: "You need to provide an email address for this account",
        },
      ])
      .then((answers) => answers.email);
  } else if (process.env.AUTOVERIFYEMAIL) {
    // console.log(mailjs);
    console.log("Creating new email...");
    let registerresult = await mailjs.createOneAccount();
    if (!registerresult.status) {
      console.log(registerresult)
      throw "Error creating email";
    }
    console.log("Created email!", registerresult.data.username);
    console.log("Logging in to email...");
    let loginresult = await mailjs.login(registerresult.data.username, registerresult.data.password);
    if (!loginresult.status) {
      throw "Error logging into email";
    }
    console.log("Logged into email!");
    return registerresult.data.username;
  }
  return (
    Math.random().toString(36).substring(7) +
    Math.random().toString(36).substring(7) +
    Math.random().toString(36).substring(7) +
    "@example.com"
  );
}

async function main() {
  const username = getUsername();
  const password = getPassword();
  const mailjs = new Mailjs();
  const email = await getEmail(mailjs);
  readFileSync(process.env.ACCOUNTS_JSON) // test that it works

  let driver: any = await new Builder().forBrowser("firefox").build();

  await driver.get("https://old.reddit.com/register");
  console.log(
    `Creating user with username: ${username} and password: ${password}`
  );

  await driver.findElement(By.id("user_reg")).sendKeys(username);
  await driver.findElement(By.id("passwd_reg")).sendKeys(password);
  await driver.findElement(By.id("passwd2_reg")).sendKeys(password);
  await driver.findElement(By.id("email_reg")).sendKeys(email);

  await inquirer.prompt([
    {
      name: "captcha",
      type: "confirm",
      message: "Press enter when you've completed the captcha",
    },
  ]);

  try {
    await driver.findElement(By.css(".c-btn.c-btn-primary.c-pull-right")).click();
  } catch (NoSuchElementError) {
    console.log("assuming user already pressed the button")
  }

  console.log("Account created!");
  if (!process.env.AUTOVERIFYEMAIL) return;

  let messages;
  while (1) {
    console.log("Waiting for verification email...");
    await wait(1000);
    messages = await mailjs.getMessages();
    if (messages.data.length > 0) {
      console.log("Email found! Verifying");
      break;
    }
  }

  let verificationEmail = await mailjs.getMessage(messages.data[0].id);
  let veriEmailhHtml = verificationEmail.data.html.join("");
  let veriLink = veriEmailhHtml.match(
    /[^"]+reddit\.com\/verification[^"]+/g
  )[0];

  await driver.get(veriLink);
  await wait(2000);
  await driver.findElement(By.css(".icon-upvote")).click();
  await wait(1000);
  await driver.quit();

  let account_list = JSON.parse(readFileSync(process.env.ACCOUNTS_JSON));
  account_list.push({ "user": username, "pass": password });
  writeFileSync(process.env.ACCOUNTS_JSON, JSON.stringify(account_list))
}

// Loop until user stops the script
async function loop() {
  await main();
  console.log("Waiting for 10min before creating another account");
  await wait(1000 * 60 * 10);
  loop();
}

loop();
